'use strict';
module.exports = (sequelize, DataTypes) => {
  var Batch = sequelize.define('Batch', {
    code: DataTypes.STRING,
    courseId: DataTypes.INTEGER,
    startDate: DataTypes.STRING,
    endDate: DataTypes.STRING,
    batchStatus: DataTypes.STRING,
    branch: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN,
    createBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  });
  Batch.associate = function(models) {
    Batch.belongsTo(models.Course, {
      foreignKey: 'courseId',
      targetKey: 'id'
    });
    Batch.hasMany(models.batch_module, {
      foreignKey: 'batchId',
      targetKey: 'id'
    });
  };
  return Batch;
};
