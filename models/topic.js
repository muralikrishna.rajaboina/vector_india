'use strict';
module.exports = (sequelize, DataTypes) => {
  var Topic = sequelize.define('Topic', {
    moduleId: DataTypes.INTEGER,
    topicName: DataTypes.STRING,
    topicCode: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    isActive: DataTypes.BOOLEAN
  });
  Topic.associate = function(models) {
    Topic.belongsTo(models.Module, {
      foreignKey: 'moduleId',
      targetKey: 'id'
    });
    Topic.hasMany(models.Subtopic, {
      foreignKey: 'topicId',
      targetKey: 'id'
    });
  };
  return Topic;
};
