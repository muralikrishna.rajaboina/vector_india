'use strict';
module.exports = (sequelize, DataTypes) => {
  var batch_module = sequelize.define('batch_module', {
    batchId: DataTypes.INTEGER,
    moduleId: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    isActive: DataTypes.BOOLEAN
  });
  batch_module.associate = function(models) {
    batch_module.belongsTo(models.Batch, {
      foreignKey: 'batchId',
      targetKey: 'id'
    });
  };
  return batch_module;
};
