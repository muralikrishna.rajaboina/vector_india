'use strict';
module.exports = (sequelize, DataTypes) => {
  var Subtopic = sequelize.define('Subtopic', {
    topicId: DataTypes.INTEGER,
    subTopicName: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    isActive: DataTypes.BOOLEAN
  });
  Subtopic.associate = function(models) {
    Subtopic.belongsTo(models.Topic, {
      foreignKey: 'topicId',
      targetKey: 'id'
    });
  };
  return Subtopic;
};
