'use strict';
module.exports = (sequelize, DataTypes) => {
  var Module = sequelize.define('Module', {
    courseId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    duration: DataTypes.INTEGER,
    description: DataTypes.STRING,
    activeSince: DataTypes.STRING,
    activeUntil: DataTypes.STRING,
    status: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  });

  Module.associate = function(models) {
    Module.belongsTo(models.Course, {
      foreignKey: 'courseId',
      targetKey: 'id'
    });
    Module.hasMany(models.Topic, {
      foreignKey: 'moduleId',
      targetKey: 'id'
    });
  };
  return Module;
};
