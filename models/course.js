'use strict';
module.exports = (sequelize, DataTypes) => {
  var Course = sequelize.define('Course', {
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    courseType: DataTypes.STRING,
    duration: DataTypes.INTEGER,
    branch: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  });
  Course.associate = function(models) {
    Course.hasMany(models.Module, {
      foreignKey: 'courseId',
      targetKey: 'id'
    });
    Course.hasMany(models.Batch, {
      foreignKey: 'courseId',
      targetKey: 'id'
    });
  };
  return Course;
};
