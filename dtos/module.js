const t = require('tcomb');

const ModuleCreateRequest = t.struct({
  courseId: t.Integer,
  moduleName: t.String,
  moduleDuration: t.Integer,
  status: t.maybe(t.String)
});

module.exports = { ModuleCreateRequest };
