const t = require('tcomb');

const TopicCreateRequest = t.struct({
  moduleId: t.Integer,
  topicName: t.String
});

module.exports = { TopicCreateRequest };
