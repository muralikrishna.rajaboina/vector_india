const t = require('tcomb');

const CourseCreateRequest = t.struct({
  courseName: t.String,
  courseDuration: t.Integer,
  courseType: t.String
});

module.exports = { CourseCreateRequest };
