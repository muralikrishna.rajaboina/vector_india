const t = require('tcomb');

const Login = t.struct({
  email: t.String,
  password: t.String
});

const LoginResult = t.struct({
  id: t.Integer,
  firstname: t.String,
  lastname: t.String,
  email: t.String,
  password: t.String,
  role: t.String
});

const LoginResponse = t.struct({
  token: t.String,
  user: LoginResult
});

module.exports = { Login, LoginResponse };
