const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const auth = require('./authentication/auth')();
const argv = require('minimist')(process.argv.slice(2));

const bodyParser = require('body-parser');
const subpath = express();

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const coursesRouter = require('./routes/course');
const modulesRouter = require('./routes/modules');
const topicsRouter = require('./routes/topics');
const batchRouter = require('./routes/batch');
const subTopicRouter = require('./routes/subtopics');
const branchRouter = require('./routes/branch');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With,Content-Type, Accept,Authorization'
  );
  next();
});

app.use(logger('dev'));
app.use('/v1', subpath);
app.use(bodyParser.json());
app.use(auth.initialize());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/courses', coursesRouter);
app.use('/modules', modulesRouter);
app.use('/topics', topicsRouter);
app.use('/batches', batchRouter);
app.use('/subtopics', subTopicRouter);
app.use('/branches', branchRouter);

// app.use(express.static(path.join(__dirname, 'build')));

// +app.get('/*', function(req, res) {
//   res.sendFile(path.join(__dirname, 'build', 'index.html'));
// });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

const domain = 'localhost';

if (argv.domain !== undefined) domain = argv.domain;
else
  console.log(
    'No --domain=xxx specified, taking default hostname "localhost".'
  );

const applicationUrl = 'http://' + domain + ':3001';

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
