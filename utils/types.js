const t = require('tcomb');

const StringNumber = t.refinement(t.String, s => !isNaN(s));

module.exports = { StringNumber };
