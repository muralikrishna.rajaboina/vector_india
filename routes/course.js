var express = require('express');
var router = express.Router();
const course = require('../models').Course;
const asyncErrorHandlerMiddleWare = require('../utils/async-custom-handler')
  .asyncErrorHandler;
const multer = require('multer');
let auth = require('../authentication/auth')();
const bodyParser = require('body-parser');
const xlstojson = require('xls-to-json-lc');
const xlsxtojson = require('xlsx-to-json-lc');
const Sequelize = require('sequelize');
const courseCreateRequest = require('../dtos/course').CourseCreateRequest;
const validate = require('../utils/validations').validateRequest;

const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function(req, file, cb) {
    cb(null, './public/images/uploads');
  },
  filename: function(req, file, cb) {
    let datetimestamp = Date.now();
    cb(
      null,
      file.fieldname +
        '-' +
        datetimestamp +
        '.' +
        file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});
const upload = multer({
  //multer settings
  storage: storage,
  fileFilter: function(req, file, callback) {
    //file filter
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('file');

router.post('/upload', async (req, res) => {
  var exceltojson;
  upload(req, res, async err => {
    // console.log(req.file);
    if (err) {
      res.json({
        error_code: 1,
        err_desc: err
      });
      return;
    }
    /** Multer gives us file info in req.file object */
    if (!req.file) {
      res.json({
        error_code: 1,
        err_desc: 'No file passed'
      });
      return;
    }
    /** Check the extension of the incoming file and
     *  use the appropriate module
     */
    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }
    //console.log(req.file.path);
    try {
      exceltojson(
        {
          input: req.file.path,
          output: null //since we don't need output.json
          // lowerCaseHeaders: true
        },
        function(err, result) {
          if (err) {
            return res.json({
              error_code: 1,
              err_desc: err,
              data: null
            });
          }
          //console.log(result);
          var result1 = result.map(function(o) {
            (o.name = o.NAME),
              (o.code = o.CODE),
              (o.duration = o.DURATION),
              (o.courseType = 'FullTime'),
              (o.isActive = true),
              (o.createdBy = 1),
              (o.updatedBy = 1);

            return o;
          });
          const coursedet = course.bulkCreate(result1);
          return res.json({
            message: 'Successfully Uploaded'
          });
        }
      );
    } catch (e) {
      res.json({
        error_code: 1,
        err_desc: 'Invalid Excel File Format'
      });
    }
  });
});

const reqValidation = (req, res, next) => {
  let validRes = validate(req.body, courseCreateRequest);
  if (validRes.isValid) next();
  else res.status(400).json(validRes.errors);
};

router.get('/', async (req, res, next) => {
  if (!req.query.course) {
    let courseDetails = await course.findAll({
      attributes: ['id', ['name', 'courseName'], 'duration', 'courseType'],
      where: {
        isActive: true
      }
    });
    res.json(courseDetails);
  } else {
    let courseDetails = await course.findAll({
      attributes: ['id', ['name', 'courseName'], 'duration', 'courseType'],
      where: {
        isActive: true,
        name: {
          $like: `%${req.query.course}%`
        }
      }
    });
    res.json(courseDetails);
  }
});

router.get('/search', async (req, res, next) => {
  if (!req.query.course) {
    let courseDetails = await course.findAll({
      attributes: [['id', 'value'], ['name', 'label']],
      where: {
        isActive: true
      }
    });
    res.json(courseDetails);
  } else {
    let courseDetails = await course.findAll({
      attributes: ['name'],
      where: {
        isActive: true,
        name: {
          $like: `%${req.query.course}%`
        }
      }
    });
    res.json(courseDetails);
  }
});

router.post(
  '/',
  reqValidation,
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    let getCourses = await course.findAll({
      attributes: ['name'],
      where: {
        isActive: true
      }
    });
    const courseList = getCourses.map(x => x.dataValues.name);
    const checkCourse = courseList.includes(req.body.courseName);
    if (!checkCourse) {
      let createCourse = await course.create({
        name: req.body.courseName,
        duration: req.body.courseDuration,
        courseType: req.body.courseType,
        code: '',
        createdBy: 1,
        updatedBy: 1,
        isActive: true
      });
      res.json(createCourse);
    } else {
      return res.status(404).json({
        message: 'Course Already Exists'
      });
    }
  })
);

router.get(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const getCourse = await course.findOne({
      attributes: [
        'id',
        ['name', 'courseName'],
        'courseType',
        ['duration', 'courseDuration']
      ],
      where: {
        id: req.params.id,
        isActive: true
      }
    });
    if (getCourse) {
      return res.json({
        couse: getCourse
      });
    } else {
      return res.json({
        message: 'Course is not Exists '
      });
    }
  })
);

router.put(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const updateCourse = await course.update(
      {
        name: req.body.courseName,
        duration: req.body.courseDuration,
        courseType: req.body.courseType,
        code: '',
        createdBy: 1,
        updatedBy: 1,
        isActive: true
      },
      {
        where: {
          id: req.params.id
        }
      }
    );
    if (updateCourse[0] > 0) {
      return res.json({
        message: 'Course is Successfully updated'
      });
    } else {
      return res.json({
        message: 'Course is not updated'
      });
    }
  })
);

router.delete('/:id', async (req, res, next) => {
  const getCourse = await course.findOne({
    attributes: [
      'id',
      ['name', 'courseName'],
      'courseType',
      ['duration', 'courseDuration']
    ],
    where: {
      id: req.params.id
    }
  });
  const deleteCourse = await course.update(
    {
      name: getCourse.dataValues.courseName,
      duration: getCourse.dataValues.courseDuration,
      courseType: getCourse.dataValues.courseType,
      code: '',
      createdBy: 1,
      updatedBy: 1,
      isActive: false
    },
    {
      where: {
        id: req.params.id
      }
    }
  );
  if (deleteCourse[0] > 0) {
    return res.json({
      message: 'Course is Successfully Deleted'
    });
  } else {
    return res.json({
      message: 'Course is not Deleted'
    });
  }
});

module.exports = router;
