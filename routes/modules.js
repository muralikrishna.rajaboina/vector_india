var express = require('express');
var router = express.Router();
const modules = require('../models').Module;
const course = require('../models').Course;
const asyncErrorHandlerMiddleWare = require('../utils/async-custom-handler')
  .asyncErrorHandler;
const multer = require('multer');
let auth = require('../authentication/auth')();
const bodyParser = require('body-parser');
const xlstojson = require('xls-to-json-lc');
const xlsxtojson = require('xlsx-to-json-lc');
const Sequelize = require('sequelize');
const moduleCreateRequest = require('../dtos/module').ModuleCreateRequest;
const validate = require('../utils/validations').validateRequest;

const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function(req, file, cb) {
    cb(null, './public/images/uploads');
  },
  filename: function(req, file, cb) {
    let datetimestamp = Date.now();
    cb(
      null,
      file.fieldname +
        '-' +
        datetimestamp +
        '.' +
        file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});
const upload = multer({
  //multer settings
  storage: storage,
  fileFilter: function(req, file, callback) {
    //file filter
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('file');

router.post('/upload', async (req, res) => {
  var exceltojson;

  upload(req, res, async err => {
    // console.log(req.file);
    if (err) {
      res.json({
        error_code: 1,
        err_desc: err
      });
      return;
    }
    /** Multer gives us file info in req.file object */
    if (!req.file) {
      res.json({
        error_code: 1,
        err_desc: 'No file passed'
      });
      return;
    }
    /** Check the extension of the incoming file and
     *  use the appropriate module
     */
    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }
    //console.log(req.file.path);
    try {
      exceltojson(
        {
          input: req.file.path,
          output: null //since we don't need output.json
          // lowerCaseHeaders: true
        },
        async (err, result) => {
          if (err) {
            return res.json({
              error_code: 1,
              err_desc: err,
              data: null
            });
          }
          //console.log(result);
          let c = await course.find({
            attributes: ['id'],
            where: {
              name: {
                $in: result.map(x => x.CourseName)
              }
            }
          });
          // console.log(c.dataValues.id);

          var result1 = result.map(function(o) {
            (o.courseId = c.dataValues.id),
              (o.name = o.Module),
              (o.duration = o.Duration),
              (o.description = o.Description),
              (o.activeSince = o.ActiveSince),
              (o.activeUntil = o.ActiveUntil),
              (o.status = o.Status),
              (o.isActive = true),
              (o.createdBy = 1),
              (o.updatedBy = 1);

            return o;
          });
          const moduleDetails = modules.bulkCreate(result1);
          return res.json({
            message: 'Successfully Uploaded'
          });
        }
      );
    } catch (e) {
      res.json({
        error_code: 1,
        err_desc: 'Invalid Excel File Format'
      });
    }
  });
});

router.get(
  '/',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    if (!req.query.module) {
      const limit = parseInt(req.query.pageSize);
      let getModules = await modules.findAndCount({
        attributes: ['id', 'name', 'duration', 'status'],
        where: { isActive: true },
        include: [
          {
            model: course,
            attributes: ['name']
          }
        ],
        limit: limit,
        offset: parseInt(limit * req.query.pageIndex)
      });

      const data = getModules.rows.map(x => ({
        id: x.dataValues.id,
        moduleName: x.name,
        duration: x.duration,
        status: x.status,
        courseId: x.Course.name
      }));
      res.json({
        count: getModules.count,
        modules: data
      });
    } else {
      const limit = parseInt(req.query.pageSize);
      let getModules = await modules.findAndCount({
        attributes: ['id', 'name', 'duration', 'status'],
        include: [
          {
            model: course,
            attributes: ['name']
          }
        ],
        limit: limit,
        offset: parseInt(limit * req.query.pageIndex),
        where: {
          isActive: true,
          name: {
            $like: `%${req.query.module}%`
          }
        }
      });
      const data = getModules.rows.map(x => ({
        id: x.dataValues.id,
        moduleName: x.name,
        duration: x.duration,
        status: x.status,
        courseId: x.Course.name
      }));
      res.json({
        count: getModules.count,
        modules: data
      });
    }
  })
);

// router.get('/searchfilter', async (req, res, next) => {
//   let getModules = await modules.findAndCount({
//     attributes: ['id', 'name', 'duration', 'status'],
//     include: [
//       {
//         model: course,
//         attributes: ['name']
//       }
//     ],
//     where: {
//       isActive: true,
//       name: {
//         $like: `%${req.query.module}%`
//       }
//     }
//   });
//   const data = getModules.rows.map(x => ({
//     id: x.dataValues.id,
//     moduleName: x.name,
//     duration: x.duration,
//     status: x.status,
//     courseId: x.Course.name
//   }));
//   res.json({
//     count: getModules.count,
//     modules: data
//   });
// });

router.get(
  '/search',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    if (req.query.course) {
      let moduleNames = await modules.findAll({
        attributes: [['id', 'value'], ['name', 'label']],
        where: {
          courseId: req.query.course
        }
      });
      res.json(moduleNames);
    } else {
      let moduleNames = await modules.findAll({
        attributes: [['id', 'value'], ['name', 'label']]
      });
      res.json(moduleNames);
    }
  })
);

const reqValidation = (req, res, next) => {
  let validRes = validate(req.body, moduleCreateRequest);
  if (validRes.isValid) next();
  else res.status(400).json(validRes.errors);
};

router.post(
  '/',
  reqValidation,
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    let getModulesOfCourse = await modules.findAll({
      attributes: ['name'],
      where: {
        courseId: req.body.courseId,
        isActive: true
      }
    });
    const moduleList = getModulesOfCourse.map(x => x.dataValues.name);
    const checkModule = moduleList.includes(req.body.moduleName);
    if (!checkModule) {
      let createModule = await modules.create({
        courseId: req.body.courseId,
        name: req.body.moduleName,
        activeSince: '',
        activeUntil: '',
        duration: req.body.moduleDuration,
        description: '',
        status: req.body.status ? req.body.status : '',
        createdBy: 1,
        updatedBy: 1,
        isActive: true
      });

      res.json(createModule);
    } else {
      return res.status(404).json({
        message: 'Module Already Exists'
      });
    }
  })
);

router.get(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const getModule = await modules.findOne({
      attributes: [
        'id',
        ['name', 'moduleName'],
        ['duration', 'moduleDuration']
      ],
      where: {
        id: req.params.id,
        isActive: true
      },
      include: [
        {
          model: course,
          attributes: ['name']
        }
      ]
    });

    let data = {
      id: getModule.dataValues.id,
      moduleName: getModule.dataValues.moduleName,
      moduleDuration: getModule.dataValues.moduleDuration,
      courseName: getModule.dataValues.Course.name
    };

    if (getModule) {
      return res.json({
        module: data
      });
    } else {
      return res.json({
        message: 'Module is not Exists '
      });
    }
  })
);

router.put(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    let getCourse = await modules.findOne({
      attributes: ['courseId'],
      where: {
        id: req.params.id
      }
    });

    const updateModule = await modules.update(
      {
        courseId: getCourse.dataValues.courseId,
        name: req.body.moduleName,
        activeSince: '',
        activeUntil: '',
        duration: req.body.moduleDuration,
        description: '',
        status: req.body.status ? req.body.status : '',
        createdBy: 1,
        updatedBy: 1,
        isActive: true
      },
      {
        where: {
          id: req.params.id
        }
      }
    );
    if (updateModule[0] > 0) {
      return res.json({
        message: 'Module is Successfully updated'
      });
    } else {
      return res.json({
        message: 'Module is not updated'
      });
    }
  })
);

router.delete('/:id', async (req, res, next) => {
  const deleteCourse = await modules.update(
    {
      isActive: false
    },
    {
      where: {
        id: req.params.id
      }
    }
  );
  if (deleteCourse[0] > 0) {
    return res.json({
      message: 'Course is Successfully Deleted'
    });
  } else {
    return res.json({
      message: 'Course is not Deleted'
    });
  }
});

module.exports = router;
