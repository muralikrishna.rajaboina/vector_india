var express = require('express');
var router = express.Router();
const branch = require('../models').Branch;
const asyncErrorHandlerMiddleWare = require('../utils/async-custom-handler')
  .asyncErrorHandler;
const multer = require('multer');
let auth = require('../authentication/auth')();
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

router.get(
  '/',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const getBranches = await branch.findAll({
      attributes: [['id', 'label'], ['name', 'name']]
    });
    res.json(getBranches);
  })
);

module.exports = router;
