var express = require('express');
var router = express.Router();
const topic = require('../models').Topic;
const modules = require('../models').Module;
const course = require('../models').Course;
const subTopic = require('../models').Subtopic;
const asyncErrorHandlerMiddleWare = require('../utils/async-custom-handler')
  .asyncErrorHandler;
const reqQueryValidate = require('../utils/req_generic_validations')
  .reqqueryvalidation;
const pagination = require('../dtos/pagination').Pagination;
const validate = require('../utils/validations').validateRequest;
const multer = require('multer');
let auth = require('../authentication/auth')();
const bodyParser = require('body-parser');
const xlstojson = require('xls-to-json-lc');
const xlsxtojson = require('xlsx-to-json-lc');
const sequelize = require('sequelize');
const moment = require('moment');

const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function(req, file, cb) {
    cb(null, './public/images/uploads');
  },
  filename: function(req, file, cb) {
    let datetimestamp = Date.now();
    cb(
      null,
      file.fieldname +
        '-' +
        datetimestamp +
        '.' +
        file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});
const upload = multer({
  //multer settings
  storage: storage,
  fileFilter: function(req, file, callback) {
    //file filter
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('file');

router.post('/upload', async (req, res) => {
  var exceltojson;
  upload(req, res, async err => {
    // console.log(req.file);
    if (err) {
      res.json({
        error_code: 1,
        err_desc: err
      });
      return;
    }
    /** Multer gives us file info in req.file object */
    if (!req.file) {
      res.json({
        error_code: 1,
        err_desc: 'No file passed'
      });
      return;
    }
    /** Check the extension of the incoming file and
     *  use the appropriate module
     */
    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }
    //console.log(req.file.path);
    try {
      exceltojson(
        {
          input: req.file.path,
          output: null //since we don't need output.json
          // lowerCaseHeaders: true
        },
        async (err, result) => {
          if (err) {
            return res.json({
              error_code: 1,
              err_desc: err,
              data: null
            });
          }
          console.log(result);

          var result1 = result.map(function(o) {
            (o.topicId = o.topicId),
              (o.subTopicName = o.subTopicName ? o.subTopicName : ''),
              (o.isActive = true),
              (o.createdBy = 1),
              (o.updatedBy = 1);

            return o;
          });
          const subTopicDetails = subTopic.bulkCreate(result1);
          return res.json({
            message: 'Successfully Uploaded'
          });
        }
      );
    } catch (e) {
      res.json({
        error_code: 1,
        err_desc: 'Invalid Excel File Format'
      });
    }
  });
});

router.get(
  '/',
  [reqQueryValidate(pagination)],
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    if (!req.query.subtopic) {
      const limit = parseInt(req.query.pageSize);
      const getAllSubtopics = await subTopic.findAndCount({
        attributes: ['id', 'topicId', 'subTopicName'],
        where: {
          isActive: true
        },
        include: [
          {
            model: topic,
            attributes: ['topicName']
          }
        ],
        limit: 10,
        offset: parseInt(limit * req.query.pageIndex)
      });
      const subTopicsList = getAllSubtopics.rows.map(x =>
        x.get({
          plain: true
        })
      );

      console.log(subTopicsList);

      const data = subTopicsList.map(x => ({
        id: x.id,
        topicName: x.Topic.topicName,
        subTopicName: x.subTopicName
      }));

      return res.json({
        count: getAllSubtopics.count,
        rows: data
      });
    } else {
      const limit = parseInt(req.query.pageSize);
      const getAllSubtopics = await subTopic.findAndCount({
        attributes: ['id', 'topicId', 'subTopicName'],
        where: {
          isActive: true
        },
        include: [
          {
            model: topic,
            attributes: ['topicName']
          }
        ],
        where: {
          isActive: true,
          subTopicName: {
            $like: `%${req.query.subtopic}%`
          }
        },
        limit: limit,
        offset: parseInt(limit * req.query.pageIndex)
      });
      const subTopicsList = getAllSubtopics.rows.map(x =>
        x.get({
          plain: true
        })
      );

      //console.log(subTopicsList);

      const data = subTopicsList.map(x => ({
        id: x.id,
        topicName: x.Topic.topicName,
        subTopicName: x.subTopicName
      }));

      return res.json({
        count: getAllSubtopics.count,
        rows: data
      });
    }
  })
);

router.get(
  '/all',
  [auth.authenticate(), reqQueryValidate(pagination)],
  async (req, res, next) => {
    if (
      !req.query.course &&
      !req.query.module &&
      !req.query.batch &&
      !req.query.topic
    ) {
      const limit = parseInt(req.query.pageSize);
      const subTopicDetails = await subTopic.findAndCount({
        attributes: [
          ['id', 'subTopicId'],
          'topicId',
          'subTopicName',
          [
            sequelize.literal(
              '(select topicName from Topics where topicId= Topics.id )'
            ),
            'topicName'
          ]
        ],
        include: [
          {
            model: topic,
            attributes: [
              'id',
              'moduleId',
              'topicCode',
              [
                sequelize.literal(
                  '(Select (Select name from Modules where Modules.id = Topics.moduleId) from Topics where Topics.id = Subtopic.topicId)'
                ),
                'moduleName'
              ]
            ],
            where: {
              id: sequelize.col('Subtopic.topicId')
            },

            include: [
              {
                model: modules,
                attributes: [
                  'id',
                  'courseId',
                  [
                    sequelize.literal(
                      '(Select (Select courseType from Courses where Courses.id = Modules.courseId) from Modules where Modules.id = Topic.moduleId)'
                    ),
                    'courseType'
                  ]
                ],
                where: {
                  id: sequelize.col('Topic.moduleId')
                }
              }
            ]
          }
        ],
        limit: limit,
        offset: parseInt(limit * req.query.pageIndex)
      });

      //return res.json(topicDetails);
      let topicsInfo = subTopicDetails.rows.map(x =>
        x.get({
          plain: true
        })
      );
      // console.log(topicsInfo.map(t => t.Topic.Module.courseType));

      let data = topicsInfo.map(x => ({
        subTopicId: x.subTopicId,
        topicId: x.topicId,
        topicName: `${x.topicName}:${x.subTopicName}`,
        subTopicName: x.subTopicName,
        topicCode: x.Topic.topicCode,
        moduleName: x.Topic.moduleName,
        courseType: x.Topic.Module.courseType
      }));

      return res.json({
        count: subTopicDetails.count,
        rows: data
      });
    } else if (
      req.query.course &&
      !req.query.module &&
      !req.query.batch &&
      !req.query.topic
    ) {
      const limit = parseInt(req.query.pageSize);
      const pageIndex = parseInt(req.query.pageIndex);
      const course = parseInt(req.query.course);
      const subTopicDetails = await subTopic.findAndCount({
        attributes: [
          ['id', 'subTopicId'],
          'topicId',
          'subTopicName',
          [
            sequelize.literal(
              '(select topicName from Topics where topicId= Topics.id )'
            ),
            'topicName'
          ]
        ],
        where: {
          topicId: {
            $in: sequelize.literal(
              '(select id from Topics where moduleId in(select id from Modules where Modules.courseId in(select Courses.id from Courses where Courses.id=' +
                course +
                ')))'
            )
          }
        },
        include: [
          {
            model: topic,
            attributes: [
              'id',
              'moduleId',
              'topicCode',
              [
                sequelize.literal(
                  '(Select (Select name from Modules where Modules.id = Topics.moduleId) from Topics where Topics.id = Subtopic.topicId)'
                ),
                'moduleName'
              ]
            ],
            where: {
              id: sequelize.col('Subtopic.topicId')
            },

            include: [
              {
                model: modules,
                attributes: [
                  'id',
                  'courseId',
                  [
                    sequelize.literal(
                      '(Select (Select courseType from Courses where Courses.id = Modules.courseId) from Modules where Modules.id = Topic.moduleId)'
                    ),
                    'courseType'
                  ]
                ],
                where: {
                  id: sequelize.col('Topic.moduleId')
                }
              }
            ]
          }
        ],
        limit: limit,
        offset: limit * pageIndex
      });

      //return res.json(topicDetails);
      let topicsInfo = subTopicDetails.rows.map(x =>
        x.get({
          plain: true
        })
      );
      // console.log(topicsInfo.map(t => t.Topic.Module.courseType));

      let data = topicsInfo.map(x => ({
        subTopicId: x.subTopicId,
        topicId: x.topicId,
        topicName: `${x.topicName}:${x.subTopicName}`,
        subTopicName: x.subTopicName,
        topicCode: x.Topic.topicCode,
        moduleName: x.Topic.moduleName,
        courseType: x.Topic.Module.courseType
      }));

      return res.json({
        count: subTopicDetails.count,
        rows: data
      });
    } else if (
      req.query.course &&
      req.query.module &&
      !req.query.batch &&
      !req.query.topic
    ) {
      const limit = parseInt(req.query.pageSize);
      const subTopicDetails = await subTopic.findAndCount({
        attributes: [
          ['id', 'subTopicId'],
          'topicId',
          'subTopicName',
          [
            sequelize.literal(
              '(select topicName from Topics where topicId= Topics.id )'
            ),
            'topicName'
          ]
        ],
        where: {
          topicId: {
            $in: sequelize.literal(
              '(select id as topicId from Topics where moduleId in (select moduleId from Topics where Topics.moduleId in(' +
                req.query.module +
                ') and Topics.moduleId in (select id from Modules where Modules.courseId=' +
                req.query.course +
                ')))'
            )
          }
        },
        include: [
          {
            model: topic,
            attributes: [
              'id',
              'moduleId',
              'topicCode',
              [
                sequelize.literal(
                  '(Select (Select name from Modules where Modules.id = Topics.moduleId) from Topics where Topics.id = Subtopic.topicId)'
                ),
                'moduleName'
              ]
            ],
            where: {
              id: sequelize.col('Subtopic.topicId')
            },

            include: [
              {
                model: modules,
                attributes: [
                  'id',
                  'courseId',
                  [
                    sequelize.literal(
                      '(Select (Select courseType from Courses where Courses.id = Modules.courseId) from Modules where Modules.id = Topic.moduleId)'
                    ),
                    'courseType'
                  ]
                ],
                where: {
                  id: sequelize.col('Topic.moduleId')
                }
              }
            ]
          }
        ],
        limit: limit,
        offset: parseInt(limit * req.query.pageIndex)
      });

      //return res.json(topicDetails);
      let topicsInfo = subTopicDetails.rows.map(x =>
        x.get({
          plain: true
        })
      );
      // console.log(topicsInfo.map(t => t.Topic.Module.courseType));

      let data = topicsInfo.map(x => ({
        subTopicId: x.subTopicId,
        topicId: x.topicId,
        topicName: `${x.topicName}:${x.subTopicName}`,
        subTopicName: x.subTopicName,
        topicCode: x.Topic.topicCode,
        moduleName: x.Topic.moduleName,
        courseType: x.Topic.Module.courseType
      }));

      return res.json({
        count: subTopicDetails.count,
        rows: data
      });
    } else if (
      req.query.course &&
      req.query.module &&
      req.query.topic &&
      !req.query.batch
    ) {
      const limit = parseInt(req.query.pageSize);
      const subTopicDetails = await subTopic.findAndCount({
        attributes: [
          ['id', 'subTopicId'],
          'topicId',
          'subTopicName',
          [
            sequelize.literal(
              '(select topicName from Topics where topicId= Topics.id )'
            ),
            'topicName'
          ]
        ],
        where: {
          topicId: req.query.topic
        },
        include: [
          {
            model: topic,
            attributes: [
              'id',
              'moduleId',
              'topicCode',
              [
                sequelize.literal(
                  '(Select (Select name from Modules where Modules.id = Topics.moduleId) from Topics where Topics.id = Subtopic.topicId)'
                ),
                'moduleName'
              ]
            ],
            where: {
              id: sequelize.col('Subtopic.topicId')
            },

            include: [
              {
                model: modules,
                attributes: [
                  'id',
                  'courseId',
                  [
                    sequelize.literal(
                      '(Select (Select courseType from Courses where Courses.id = Modules.courseId) from Modules where Modules.id = Topic.moduleId)'
                    ),
                    'courseType'
                  ]
                ],
                where: {
                  id: sequelize.col('Topic.moduleId')
                }
              }
            ]
          }
        ],
        limit: limit,
        offset: parseInt(limit * req.query.pageIndex)
      });

      //return res.json(topicDetails);
      let topicsInfo = subTopicDetails.rows.map(x =>
        x.get({
          plain: true
        })
      );
      // console.log(topicsInfo.map(t => t.Topic.Module.courseType));

      let data = topicsInfo.map(x => ({
        subTopicId: x.subTopicId,
        topicId: x.topicId,
        topicName: `${x.topicName}:${x.subTopicName}`,
        subTopicName: x.subTopicName,
        topicCode: x.Topic.topicCode,
        moduleName: x.Topic.moduleName,
        courseType: x.Topic.Module.courseType
      }));

      return res.json({
        count: subTopicDetails.count,
        rows: data
      });
    } else if (
      req.query.course &&
      req.query.module &&
      req.query.batch &&
      !req.query.topic
    ) {
      const limit = parseInt(req.query.pageSize);
      const subTopicDetails = await subTopic.findAndCount({
        attributes: [
          ['id', 'subTopicId'],
          'topicId',
          'subTopicName',
          [
            sequelize.literal(
              '(select topicName from Topics where topicId= Topics.id )'
            ),
            'topicName'
          ],
          [
            sequelize.literal(
              '(select startDate from Batches where Batches.id=' +
                req.query.batch +
                ')'
            ),
            'startDate'
          ],
          [
            sequelize.literal(
              '(select endDate from Batches where Batches.id=' +
                req.query.batch +
                ')'
            ),
            'endDate'
          ]
        ],
        where: {
          topicId: {
            $in: sequelize.literal(
              '(select id as topicId from Topics where moduleId in (select moduleId from Topics where Topics.moduleId in(' +
                req.query.module +
                ') and Topics.moduleId in (select id from Modules where Modules.courseId=' +
                req.query.course +
                ')))'
            )
          }
        },
        include: [
          {
            model: topic,
            attributes: [
              'id',
              'moduleId',
              'topicCode',
              [
                sequelize.literal(
                  '(Select (Select name from Modules where Modules.id = Topics.moduleId) from Topics where Topics.id = Subtopic.topicId)'
                ),
                'moduleName'
              ]
            ],
            where: {
              id: sequelize.col('Subtopic.topicId')
            },

            include: [
              {
                model: modules,
                attributes: [
                  'id',
                  'courseId',
                  [
                    sequelize.literal(
                      '(Select (Select courseType from Courses where Courses.id = Modules.courseId) from Modules where Modules.id = Topic.moduleId)'
                    ),
                    'courseType'
                  ]
                ],
                where: {
                  id: sequelize.col('Topic.moduleId')
                }
              }
            ]
          }
        ],
        limit: limit,
        offset: parseInt(limit * req.query.pageIndex)
      });

      //return res.json(topicDetails);
      let topicsInfo = subTopicDetails.rows.map(x =>
        x.get({
          plain: true
        })
      );
      // console.log(topicsInfo.map(t => t.Topic.Module.courseType));

      let data = topicsInfo.map(x => ({
        subTopicId: x.subTopicId,
        topicId: x.topicId,
        topicName: `${x.topicName}:${x.subTopicName}`,
        subTopicName: x.subTopicName,
        topicCode: x.Topic.topicCode,
        moduleName: x.Topic.moduleName,
        courseType: x.Topic.Module.courseType,
        startDate: x.startDate,
        endDate: x.endDate,
        totalDays: moment
          .duration(
            moment(x.endDate, 'YYYY-MM-DD').diff(
              moment(x.startDate, 'YYYY-MM-DD')
            )
          )
          .asDays()
      }));

      return res.json({
        count: subTopicDetails.count,
        rows: data
      });
    }
  }
);

router.get(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const getSubtopic = await subTopic.findOne({
      attributes: ['id', 'topicId', 'subTopicName'],
      where: {
        isActive: true,
        id: req.params.id
      },
      include: [
        {
          model: topic,
          attributes: ['topicName']
        }
      ]
    });
    const data = {
      id: getSubtopic.dataValues.id,
      topicName: getSubtopic.dataValues.Topic.topicName,
      subTopicName: getSubtopic.dataValues.subTopicName
    };

    return res.json({
      rows: data
    });
  })
);

router.get('/search', async (req, res, next) => {
  if (!req.query.topic) {
    let topicNames = await subTopic.findAndCount({
      attributes: [['id', 'label'], ['subTopicName', 'name']]
    });

    return res.json({
      count: topicNames.rows.length,
      subTopics: topicNames.rows
    });
  } else if (req.query.topic) {
    let topicNames = await subTopic.findAndCount({
      attributes: [['id', 'label'], ['subTopicName', 'name']],
      where: {
        topicId: req.query.topic
      }
    });
    return res.json({
      count: topicNames.rows.length,
      subTopics: topicNames.rows
    });
  }
});

router.post(
  '/',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    let getTopicName = await topic.findOne({
      attributes: ['topicName'],
      where: {
        id: req.body.topicId
      }
    });

    let topicName = getTopicName.dataValues.topicName;
    console.log(topicName);

    let getsubTopics = await subTopic.findAll({
      attribute: ['id', 'subTopicName'],
      where: {
        topicId: req.body.topicId
      }
    });

    console.log(getsubTopics.map(x => x.dataValues.id)[0]);

    let subTopicsList = getsubTopics.map(x => x.dataValues.subTopicName);
    console.log(subTopicsList.length);
    console.log(subTopicsList);
    console.log(subTopicsList.subTopicName === topicName);
    if (subTopicsList.length === 1 && subTopicsList[0] === topicName) {
      let deleteSubTopic = await subTopic.destroy({
        where: {
          id: getsubTopics.map(x => x.dataValues.id)[0]
        }
      });
      console.log(deleteSubTopic);
      let createSubtopic = await subTopic.create({
        id: getsubTopics.map(x => x.dataValues.id)[0],
        topicId: req.body.topicId,
        subTopicName: req.body.subTopicName,
        createdBy: 1,
        updatedBy: 1,
        isActive: true
      });
      return res.json(createSubtopic);
    } else {
      let createSubtopic = await subTopic.create({
        topicId: req.body.topicId,
        subTopicName: req.body.subTopicName,
        createdBy: 1,
        updatedBy: 1,
        isActive: true
      });
      return res.json(createSubtopic);
    }
  })
);

router.put(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const updateSubTopic = await subTopic.update(
      {
        topicId: req.body.topicId,
        subTopicName: req.body.subTopicName,
        createdBy: 1,
        updatedBy: 1,
        isActive: true
      },
      {
        where: {
          id: req.params.id
        }
      }
    );
    if (updateSubTopic[0] > 0) {
      return res.json({
        message: 'SubTopic is Successfully updated'
      });
    } else {
      return res.json({
        message: 'SubTopic is not updated'
      });
    }
  })
);

router.delete('/:id', async (req, res, next) => {
  const deleteSubtopic = await subTopic.update(
    {
      isActive: false
    },
    {
      where: {
        id: req.params.id
      }
    }
  );
  if (deleteSubtopic[0] > 0) {
    return res.json({
      message: 'SubTopic is Successfully Deleted'
    });
  } else {
    return res.json({
      message: 'SubTopic is not Deleted'
    });
  }
});

module.exports = router;
