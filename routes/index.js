var express = require('express');
var router = express.Router();
const user = require('../models').User;
const jwt = require('jwt-simple');
const cfg = require('../authentication/jwt_config');
const login = require('../dtos/login').Login;
const validate = require('../utils/validations').validateRequest;
const asyncErrorHandlerMiddleWare = require('../utils/async-custom-handler')
  .asyncErrorHandler;

const reqValidation = (req, res, next) => {
  let validRes = validate(req.body, login);
  if (validRes.isValid) next();
  else res.status(400).json(validRes.errors);
};

/* GET home page. */
// router.get('/', async function(req, res, next) {
//   res.json('WELCOME TO VECTOR INDIA');
// });

router.post(
  '/token',
  reqValidation,
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    console.log(req.body);
    let getUser = await user.find({
      attributes: ['id', 'firstName', 'lastName', 'email'],
      where: {
        email: req.body.email,
        password: req.body.password
      }
    });
    if (getUser) {
      res.json({
        token: jwt.encode({ id: getUser.id }, cfg.jwtSecret),
        user: getUser
      });
    } else {
      res.sendStatus(401);
    }
  })
);

module.exports = router;
