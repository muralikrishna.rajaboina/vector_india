var express = require('express');
var router = express.Router();
const batch = require('../models').Batch;
const course = require('../models').Course;
const batch_module = require('../models').batch_module;

const asyncErrorHandlerMiddleWare = require('../utils/async-custom-handler')
  .asyncErrorHandler;
const multer = require('multer');
let auth = require('../authentication/auth')();
const bodyParser = require('body-parser');
const xlstojson = require('xls-to-json-lc');
const xlsxtojson = require('xlsx-to-json-lc');
const sequelize = require('sequelize');
const moment = require('moment');

const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function (req, file, cb) {
    cb(null, './public/images/uploads');
  },
  filename: function (req, file, cb) {
    let datetimestamp = Date.now();
    cb(
      null,
      file.fieldname +
      '-' +
      datetimestamp +
      '.' +
      file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});
const upload = multer({
  //multer settings
  storage: storage,
  fileFilter: function (req, file, callback) {
    //file filter
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('file');

router.post('/upload', async (req, res) => {
  var exceltojson;
  upload(req, res, async err => {
    // console.log(req.file);
    if (err) {
      res.json({
        error_code: 1,
        err_desc: err
      });
      return;
    }
    /** Multer gives us file info in req.file object */
    if (!req.file) {
      res.json({
        error_code: 1,
        err_desc: 'No file passed'
      });
      return;
    }
    /** Check the extension of the incoming file and
     *  use the appropriate module
     */
    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }
    //console.log(req.file.path);
    try {
      exceltojson({
          input: req.file.path,
          output: null //since we don't need output.json
          // lowerCaseHeaders: true
        },
        async (err, result) => {
          if (err) {
            return res.json({
              error_code: 1,
              err_desc: err,
              data: null
            });
          }

          var result1 = result.map(function (o) {
            (o.code = o.BatchCode),
            (o.courseId = 1),
            (o.startDate = o.StartDate),
            (o.endDate = o.EndDate),
            (o.batchStatus = o.BatchStatus),
            (o.branch = o.Branch),
            (o.isActive = true),
            (o.createBy = 1),
            (o.updatedBy = 1);

            return o;
          });
          const batchDetails = batch.bulkCreate(result1);
          return res.json({
            message: 'Successfully Uploaded'
          });
        }
      );
    } catch (e) {
      res.json({
        error_code: 1,
        err_desc: 'Invalid Excel File Format'
      });
    }
  });
});

router.get('/search', async (req, res, next) => {
  if (!req.query.branch) {
    let batchDetails = await batch.findAll({
      attributes: [
        ['id', 'value'],
        ['code', 'label']
      ],
      where: {
        code: {
          $like: `%${req.query.code}%`
        }
      }
    });
    res.json(batchDetails);
  } else {
    let batchDetails = await batch.findAll({
      attributes: [
        ['id', 'value'],
        ['code', 'label']
      ],
      where: {
        branch: req.query.branch,
        code: {
          $like: `%${req.query.code}%`
        }
      }
    });
    res.json(batchDetails);
  }
});

router.get(
  '/',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const limit = parseInt(req.query.pageSize);
    const pageIndex = parseInt(req.query.pageIndex);
    const getBatch = await batch.findAndCount({
      attributes: [
        'id',
        'courseId',
        'code',
        'startDate',
        'endDate',
        'batchStatus',
        'branch', [
          sequelize.literal(
            '(Select name from Courses where Courses.id = Batch.CourseId)'
          ),
          'c'
        ]
      ],


      include: [{
        model: batch_module,
        attributes: [
          [
            sequelize.literal(
              '(Select name from Modules where Modules.id = batch_modules.moduleId)'
            ),
            'moduleName'
          ]

        ]

      }],
      where: {
        isActive: true
      },
      limit: limit,
      offset: parseInt(limit * req.query.pageIndex)
    });
    let d = getBatch.rows.map(x => x.get({
      plain: true
    }));

    console.log(getBatch.length);
    d.map(x => x.batch_modules.map(x => x));

    const data = d.map(x => ({
      id: x.id,
      courseId: x.courseId,
      code: x.code,
      startDate: x.startDate,
      endDate: x.endDate,
      batchStatus: 'Open',
      branch: x.branch,
      modules: x.batch_modules
        .map(y => y)
        .map(m => m.moduleName)
        .join(),
      courseName: x.c
    }));

    //console.log(d);
    // console.log(getBatch.rows.length + limit * pageIndex);
    return res.json({
      count: getBatch.count,
      batches: data
    });
  })
);

router.get(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const getBatch = await batch.findOne({
      attributes: [
        'id',
        'courseId',
        'code',
        'startDate',
        'endDate',
        'batchStatus',
        'branch', [
          sequelize.literal(
            '(Select name from Courses where Courses.id = Batch.courseId)'
          ),
          'courseName'
        ]
      ],
      where: {
        isActive: true,
        id: req.params.id
      },
      include: [{
        model: batch_module,
        attributes: [
          [
            sequelize.literal(
              '(Select name from Modules where Modules.id = batch_modules.moduleId)'
            ),
            'moduleName'
          ]
        ]
      }]
    });

    //console.log(getBatch.dataValues.batch_modules.map(x => x.dataValues.moduleName).join());

    if (getBatch) {

      const data = {
        id: getBatch.dataValues.id,
        courseId: getBatch.dataValues.courseId,
        code: getBatch.dataValues.code,
        startDate: getBatch.dataValues.startDate,
        endDate: getBatch.dataValues.endDate,
        batchStatus: getBatch.dataValues.batchStatus,
        branch: getBatch.dataValues.branch,
        courseName: getBatch.dataValues.courseName,
        modules: getBatch.dataValues.batch_modules.map(x => x.dataValues.moduleName).join()
      }

      return res.json(data);
    } else {
      return res.json("Batch is Available")
    }


  })
);

router.post(
  '/',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const courseName = await course.findOne({
      attributes: ['name', 'duration'],
      where: {
        id: req.body.course
      }
    });

    //console.log(courseName.dataValues.duration);

    const year = req.body.startDate.slice(2, 4);
    //console.log(year);

    startDate = new Date(req.body.startDate.replace(/-/g, '/'));
    var endDate = '',
      noOfDaysToAdd = courseName.dataValues.duration,
      count = 0;
    while (count < noOfDaysToAdd) {
      endDate = new Date(startDate.setDate(startDate.getDate() + 1));
      if (endDate.getDay() != 0) {
        //Date.getDay() gives weekday starting from 0(Sunday) to 6(Saturday)
        count++;
      }
    }
    //console.log(endDate);

    const batchList = await batch.findAll({
      attributes: ['code', 'branch'],
      where: {
        branch: req.body.branch,
        code: {
          $like: `%${year}%`
        }
      }
    });

    const b = batchList.map(x => x.dataValues.code);
    //console.log(b.length);

    const courseN = courseName.dataValues.name;
    //console.log(req.body.branch.charAt(0));

    const batchcreate = await batch.create({
      code: `V${year}${req.body.branch.charAt(0)}${courseN.charAt(
        0
      )}${b.length + 1}`,
      courseId: req.body.course,
      startDate: req.body.startDate,
      endDate: endDate.toISOString().substr(0, 10),
      batchStatus: 'Open',
      branch: req.body.branch,
      isActive: true,
      createBy: 1,
      updatedBy: 1
    });
    //console.log(req.body.modules.map(x => x));

    let bm = req.body.modules.map(function (n) {
      (n.batchId = batchcreate.id),
      (n.createdBy = 1),
      (n.updatedBy = 1),
      (n.isActive = true);
      return n;
    });

    //console.log(bm);

    const batchModuleCreate = await batch_module.bulkCreate(bm);

    return res.json(batchcreate);
  })
);

router.put(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const updateBatch = await batch.update({
      startDate: req.body.startDate,
      endDate: req.body.endDate
    }, {
      where: {
        id: req.params.id
      }
    });
    if (updateBatch[0] > 0) {
      return res.json({
        message: 'Batch is Successfully Updated'
      });
    } else {
      return res.json({
        message: 'Batch is not Updated'
      });
    }
  })
);


router.delete(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    const deleteBatch = await batch.update({
        isActive: false
      }, {
        where: {
          id: req.params.id
        }
      }


    );

    const deleteBatchModules = await batch_module.update({
        isActive: false
      }, {
        where: {
          batchId: req.params.id
        }
      }

    )
    if (deleteBatch[0] > 0) {
      return res.json({
        message: 'Batch is Successfully Deleted'
      });
    } else {
      return res.json({
        message: 'Batch is not Deleted'
      });
    }
  })
);


module.exports = router;