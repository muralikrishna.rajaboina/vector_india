var express = require('express');
var router = express.Router();
const topic = require('../models').Topic;
const modules = require('../models').Module;
const course = require('../models').Course;
const subTopic = require('../models').Subtopic;
const asyncErrorHandlerMiddleWare = require('../utils/async-custom-handler')
  .asyncErrorHandler;
const reqQueryValidate = require('../utils/req_generic_validations')
  .reqqueryvalidation;
const pagination = require('../dtos/pagination').Pagination;
const validate = require('../utils/validations').validateRequest;
const multer = require('multer');
let auth = require('../authentication/auth')();
const bodyParser = require('body-parser');
const xlstojson = require('xls-to-json-lc');
const xlsxtojson = require('xlsx-to-json-lc');
const sequelize = require('sequelize');
const moment = require('moment');
const topicCreateRequest = require('../dtos/topic').TopicCreateRequest;

const reqValidation = (req, res, next) => {
  let validRes = validate(req.body, topicCreateRequest);
  if (validRes.isValid) next();
  else res.status(400).json(validRes.errors);
};

// router.get('/searchfilter', async (req, res, next) => {
//   let getTopicDetails = await topic.findAndCount({
//     attributes: ['id', 'topicName', 'topicCode'],
//     include: [
//       {
//         model: modules,
//         attributes: ['name'],
//         include: [
//           {
//             model: course,
//             attributes: ['name']
//           }
//         ]
//       }
//     ],
//     where: {
//       isActive: true,
//       topicName: {
//         $like: `%${req.query.topic}%`
//       }
//     }
//   });
//   const topicDet = getTopicDetails.rows.map(x =>
//     x.get({
//       plain: true
//     })
//   );

//   let data = topicDet.map(t => ({
//     id: t.id,
//     topicName: t.topicName,
//     topicCode: t.topicCode,
//     moduleId: t.Module.name,
//     courseName: t.Module.Course.name
//   }));

//   res.json({
//     count: getTopicDetails.count,
//     topics: data
//   });
// });

router.get('/search', async (req, res, next) => {
  if (!req.query.module && !req.query.topic) {
    let topicNames = await topic.findAndCount({
      attributes: [['id', 'label'], ['topicName', 'name']]
    });

    return res.json({
      topics: topicNames.rows
    });
  } else if (!req.query.module && req.query.topic) {
    let topicNames = await topic.findAndCount({
      attributes: [['id', 'label'], ['topicName', 'name']],
      where: {
        topicName: {
          $like: `%${req.query.topic}%`
        }
      }
    });

    return res.json({
      topics: topicNames.rows
    });
  } else if (req.query.module && req.query.topic) {
    let topicNames = await topic.findAndCount({
      attributes: [['id', 'label'], ['topicName', 'name']],
      where: {
        moduleId: req.query.module,
        topicName: {
          $like: `%${req.query.topic}%`
        }
      }
    });

    return res.json({
      topics: topicNames.rows
    });
  }
});

router.get(
  '/',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    if (!req.query.topic) {
      const limit = parseInt(req.query.pageSize);
      const pi = req.query.pageIndex;

      let getTopicDetails = await topic.findAndCount({
        attributes: ['id', 'topicName', 'topicCode'],
        where: {
          isActive: true
        },
        include: [
          {
            model: modules,
            attributes: ['name'],
            include: [
              {
                model: course,
                attributes: ['name']
              }
            ]
          }
        ],
        limit: limit,
        offset: parseInt(limit * pi)
      });
      const topicDet = getTopicDetails.rows.map(x =>
        x.get({
          plain: true
        })
      );

      let data = topicDet.map(t => ({
        id: t.id,
        topicName: t.topicName,
        topicCode: t.topicCode,
        moduleId: t.Module.name,
        courseName: t.Module.Course.name
      }));

      res.json({
        count: getTopicDetails.count,
        topics: data
      });
    } else {
      // const limit = parseInt(req.query.pageSize);
      // const pi = req.query.pageIndex;
      let getTopicDetails = await topic.findAndCount({
        attributes: ['id', 'topicName', 'topicCode'],
        include: [
          {
            model: modules,
            attributes: ['name'],
            include: [
              {
                model: course,
                attributes: ['name']
              }
            ]
          }
        ],
        // limit: limit,
        // offset: parseInt(limit * pi),
        where: {
          isActive: true,
          topicName: {
            $like: `%${req.query.topic}%`
          }
        }
      });
      const topicDet = getTopicDetails.rows.map(x =>
        x.get({
          plain: true
        })
      );

      let data = topicDet.map(t => ({
        id: t.id,
        topicName: t.topicName,
        topicCode: t.topicCode,
        moduleId: t.Module.name,
        courseName: t.Module.Course.name
      }));

      res.json({
        count: getTopicDetails.count,
        topics: data
      });
    }
  })
);

router.get(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    let getTopicDetails = await topic.findOne({
      attributes: ['id', 'topicName', 'topicCode'],
      where: {
        id: req.params.id,
        isActive: true
      },

      include: [
        {
          model: modules,
          attributes: ['name'],
          include: [
            {
              model: course,
              attributes: ['name']
            }
          ]
        }
      ]
    });
    console.log(getTopicDetails);

    let data = {
      id: getTopicDetails.dataValues.id,
      topicName: getTopicDetails.dataValues.topicName,
      topicCode: getTopicDetails.dataValues.topicCode,
      moduleId: getTopicDetails.dataValues.Module.name,
      courseName: getTopicDetails.dataValues.Module.Course.name
    };

    res.json({
      count: getTopicDetails.count,
      topics: data
    });
  })
);

router.post(
  '/',
  reqValidation,
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    let moduleName = await modules.findOne({
      attributes: ['name'],
      where: {
        id: req.body.moduleId
      }
    });
    console.log(moduleName.dataValues.name);

    const topicsOfModule = await topic.findAll({
      attributes: ['topicName'],
      where: {
        moduleId: req.body.moduleId,
        isActive: true
      }
    });

    const topicsList = topicsOfModule.map(x => x.dataValues.topicName);
    const checkTopic = topicsList.includes(req.body.topicName);

    if (!checkTopic) {
      let createTopic = await topic.create({
        moduleId: req.body.moduleId,
        topicName: req.body.topicName,
        topicCode:
          moduleName.dataValues.name.slice(0, 3) +
          '-' +
          req.body.topicName
            .split(/[\s;]+/)
            .map(x => x.slice(0, 1))
            .join(''),

        createdBy: 1,
        updatedBy: 1,
        isActive: true
      });

      let createSubtopic = await subTopic.create({
        topicId: createTopic.id,
        subTopicName: req.body.topicName,
        createdBy: 1,
        updatedBy: 1,
        isActive: true
      });

      return res.json({
        topic: createTopic
      });
    } else {
      return res.status(404).json({
        message: 'Topic already exists'
      });
    }

    console.log(createSubtopic);
  })
);

router.put(
  '/:id',
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    let getModuleId = await topic.findOne({
      attributes: ['moduleId'],
      where: {
        id: req.params.id
      }
    });
    console.log(getModuleId.dataValues.moduleId);

    let moduleName = await modules.findOne({
      attributes: ['name'],
      where: {
        id: getModuleId.dataValues.moduleId
      }
    });
    //console.log(moduleName.dataValues.name);

    const updateTopic = await topic.update(
      {
        moduleId: getModuleId.dataValues.moduleId,

        topicName: req.body.topicName,
        topicCode:
          moduleName.dataValues.name.slice(0, 3) +
          '-' +
          req.body.topicName
            .split(/[\s;]+/)
            .map(x => x.slice(0, 1))
            .join(''),
        createdBy: 1,
        updatedBy: 1,
        isActive: true
      },
      {
        where: {
          id: req.params.id
        }
      }
    );
    if (updateTopic[0] > 0) {
      return res.json({
        message: 'Topic is Successfully updated'
      });
    } else {
      return res.json({
        message: 'Topic is not updated'
      });
    }
  })
);

const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function(req, file, cb) {
    cb(null, './public/images/uploads');
  },
  filename: function(req, file, cb) {
    let datetimestamp = Date.now();
    cb(
      null,
      file.fieldname +
        '-' +
        datetimestamp +
        '.' +
        file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});
const upload = multer({
  //multer settings
  storage: storage,
  fileFilter: function(req, file, callback) {
    //file filter
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('file');

router.post('/upload', async (req, res) => {
  var exceltojson;
  upload(req, res, async err => {
    // console.log(req.file);
    if (err) {
      res.json({
        error_code: 1,
        err_desc: err
      });
      return;
    }
    /** Multer gives us file info in req.file object */
    if (!req.file) {
      res.json({
        error_code: 1,
        err_desc: 'No file passed'
      });
      return;
    }
    /** Check the extension of the incoming file and
     *  use the appropriate module
     */
    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }
    //console.log(req.file.path);
    try {
      exceltojson(
        {
          input: req.file.path,
          output: null //since we don't need output.json
          // lowerCaseHeaders: true
        },
        async (err, result) => {
          if (err) {
            return res.json({
              error_code: 1,
              err_desc: err,
              data: null
            });
          }
          // console.log(result);

          let c = await modules.findAndCount({
            attributes: ['id', 'name'],
            where: {
              name: {
                $in: result.map(x => x.Module)
              }
            }
          });

          const sd = c.rows.map(y =>
            y.get({
              plain: true
            })
          );

          //console.log(sd)

          var result1 = result.map(function(o) {
            (o.moduleId = sd.find(x => x.name == o.Module).id),
              (o.topicName = o.Topic),
              ((o.topicCode =
                o.Module.slice(0, 3) +
                '-' +
                o.topicName
                  .split(/[\s;]+/)
                  .map(x => x.slice(0, 1))
                  .join('')),
              (o.isActive = true),
              (o.createdBy = 1),
              (o.updatedBy = 1));

            return o;
          });
          const topicDetails = topic.bulkCreate(result1);
          return res.json({
            message: 'Successfully Uploaded'
          });
        }
      );
    } catch (e) {
      res.json({
        error_code: 1,
        err_desc: 'Invalid Excel File Format'
      });
    }
  });
});

router.delete('/:id', async (req, res, next) => {
  const deleteTopic = await topic.update(
    {
      isActive: false
    },
    {
      where: {
        id: req.params.id
      }
    }
  );
  if (deleteTopic[0] > 0) {
    return res.json({
      message: 'Course is Successfully Deleted'
    });
  } else {
    return res.json({
      message: 'Course is not Deleted'
    });
  }
});

module.exports = router;
