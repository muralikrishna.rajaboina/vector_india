let passport = require('passport');
let passportJWT = require('passport-jwt');
let usr = require('../models').User;
let cfg = require('./jwt_config.js');
let ExtractJWT = passportJWT.ExtractJwt;
let Strategy = passportJWT.Strategy;
let params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
};
module.exports = function() {
  let strategy = new Strategy(params, function(payload, done) {
    const user = usr.find({
      attributes: ['id', 'firstName', 'lastName', 'email', 'role'],
      where: { id: payload.id }
    });
    user.then(u => {
      //console.log(u);
      if (u) {
        return done(null, {
          id: u.id,
          firstName: u.firstName,
          lastName: u.lastName,
          role: u.role,
          email: u.email
        });
      } else {
        return done(new Error('User not found'), null);
      }
    });
  });
  passport.use(strategy);
  return {
    initialize: function() {
      return passport.initialize();
    },
    authenticate: function() {
      return passport.authenticate('jwt', cfg.jwtSession);
    }
  };
};
